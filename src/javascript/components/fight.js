import { renderArena } from './arena'

export let moves = {};

export let firstFighterInBlock = false;
export let secondFighterInBlock = false;

let firstFighterCanCrit = true;
let secondFighterCanCrit = true;

let playerOne = {};
let playerTwo = {};


export async function fight(firstFighter, secondFighter) {
  if (firstFighter === secondFighter) {
    firstFighter = { ... firstFighter };
  }

  playerOne = { ...firstFighter };
  playerTwo = { ...secondFighter };
  playerOne.inBlock = firstFighterInBlock;
  playerTwo.inBlock = secondFighterInBlock;

  const myWrapepr = fightWrapper([firstFighter, secondFighter]);
  document.addEventListener('keydown', myWrapepr, false);
  document.addEventListener('keyup', myWrapepr, false);

  return new Promise((resolve) => {
    if (firstFighter.health <= 0 || secondFighter.health <= 0) {
      document.removeEventListener('keyup', myWrapepr, false);
      document.removeEventListener('keydown', myWrapepr, false);
      if (secondFighter.health <= 0) {
        resolve(firstFighter);
      } else {
        resolve(secondFighter);
      }
    }
  });
}

function fightWrapper(fighters) {
  const [firstFighter, secondFighter] = fighters;

  const handleAction = function(event) {
    moves[event.code] = event.type == 'keydown';
    // console.log(moves);
    // console.log(playerOne, playerTwo);

    // firstFighter controls
    if (moves.KeyA && !firstFighterInBlock) {
      secondFighter.health -= getDamage(firstFighter, secondFighter);
      console.log('firstFighter attacks secondFighter')
    }
    if (moves.KeyD) {
      firstFighterInBlock = true;
      // console.log('firstFighter in block', firstFighterInBlock);
    } else if (!moves.KeyD && event.code === 'KeyD') {
      firstFighterInBlock = false;
      // console.log('firstFighter in block', firstFighterInBlock);
    }
    if (moves.KeyQ && moves.KeyW && moves.KeyE && firstFighterCanCrit && !firstFighterInBlock) {
      firstFighterCanCrit = false;
      secondFighter.health -= getComboDamage(firstFighter);
      freezeFirstFighterCombo();
      // console.log('firstFighter crits');
    }

    // secondFighter controls
    if (moves.KeyJ && !secondFighterInBlock) {
      firstFighter.health -= getDamage(secondFighter, firstFighter);
      // console.log('secondFighter attacks firstFighter');
    }
    if (moves.KeyL) {
      secondFighterInBlock = true;
      // console.log('secondFighter in block', secondFighterInBlock);
    } else if (!moves.KeyL && event.code === 'KeyL') {
      secondFighterInBlock = false;
      // console.log('secondFighter in block', secondFighterInBlock);
    }
    if (moves.KeyU && moves.KeyI && moves.KeyO && secondFighterCanCrit && !secondFighterInBlock) {
      secondFighterCanCrit = false;
      firstFighter.health -= getComboDamage(secondFighter);
      freezeSecondFighterCombo();
      // console.log('secondFighter crits');
    }
    document.removeEventListener('keyup', handleAction, false);
    document.removeEventListener('keydown', handleAction, false);
    renderArena([firstFighter, secondFighter])
  };
  return handleAction;
}

export function freezeFirstFighterCombo() {
  setTimeout(() => firstFighterCanCrit = true, 10000);
}

export function freezeSecondFighterCombo() {
  setTimeout(() => secondFighterCanCrit = true, 10000);
}

export function getComboDamage(attacker) {
  const attack = attacker.attack;
  return 2 * attack;
}

export function getDamage(attacker, defender) {
  const power = getHitPower(attacker) - getBlockPower(defender);
  if (getBlockPower(defender) > getHitPower(attacker) || power <= 0) {
    return 0;
  }
  return power;
}

export function getHitPower(fighter) {
  const attack = fighter.attack;
  const randomNumber = Math.random() + 1;
  const power = attack * randomNumber;
  return power;
}

export function getBlockPower(fighter) {
  const randomNumber =  Math.random() + 1;
  if (fighter.name === playerOne.name) {
    const defense = playerOne.defense;
    if (playerOne.inBlock) {
      const power = defense * randomNumber;
      return power;
    }
    return defense;
  }
  if (fighter.name === playerTwo.name) {
    const defense = playerTwo.defense;
    if (playerTwo.inBlock) {
      console.log('player two is getting damage in block')
      const power = defense * randomNumber;
      return power;
    }
    return defense;
  }
}
