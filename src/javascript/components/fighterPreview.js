import { createElement } from '../helpers/domHelper';
// import { createImage } from '../components/fightersView';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if(fighter) {
    const imageElement = createFighterImage(fighter);
    fighterElement.append(imageElement);
    const infoElement = createFighterInfo(fighter);
    fighterElement.append(infoElement);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    title: name,
    alt: name,
    attributes,
  });

  return imgElement;
}

export function createFighterInfo(fighter) {
  // const { name, health, attack, defense } = fighter;
  const infoUlElement = createElement({
    tagName: 'ul',
    className: 'fighter-info___list',
  });

  const fighterClone = Object.assign({}, fighter);
  delete fighterClone.source;
  delete fighterClone._id;

  for (let [key, value] of Object.entries(fighterClone)) {
    const infoLiElement = createElement({
      tagName: 'li',
      className: `fighter-info__list-element__${key}`,
    });
    infoLiElement.appendChild(document.createTextNode(key !== 'name' ? `${key}: ${value}`: `${value}`));
    infoUlElement.append(infoLiElement);
  }

  return infoUlElement;
}
