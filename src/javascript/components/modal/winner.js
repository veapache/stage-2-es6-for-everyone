import { showModal } from './modal'

export function showWinnerModal(fighter) {
  // call showModal function 
  showModal({title: 'GAME OVER', bodyElement: `The winner is ${fighter.name}`})
}
